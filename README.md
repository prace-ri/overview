# PRACE CodeVault

PRACE CodeVault is an open repository containing various high
performance computing code samples. The project aims to support
self-learning of HPC programming and will be used as an Open platform
for the HPC community to share example code snippets, proof-of-concept
codes and so forth.  

CodeVault contains training material from PRACE partners, as well as
example codes of common HPC kernels such as dense and sparse linear
algebra, spectral and N-body methods, structured and unstructured
grids, Monte Carlo methods and parallel I/O. The code samples are
published as open source and can be used both for educational purposes
and as parts of real application suites (as permitted by particular
license).  

CodeVault consist of several Gitlab-projects which are organized into groups
and subgroups.


## HPC kernels

HPC kernels are under [hpc-kernels](https://gitlab.com/prace-ri/hpc-kernels) group where each individual
kernel is a Gitlab project

 * [Dense linear algebra](https://gitlab.com/prace-ri/hpc-kernels/dense_linear_algebra)
 * [Distributed dense linear algebra](https://gitlab.com/prace-ri/hpc-kernels/distributed_dense_linear_algebra)
 * [Monte Carlo methods](https://gitlab.com/prace-ri/hpc-kernels/monte_carlo_methods)
 * [N-body methods](https://gitlab.com/prace-ri/hpc-kernels/n-body_methods)
 * [Parallel I/O](https://gitlab.com/prace-ri/hpc-kernels/parallel_io)
 * [Sparse linear algebra](https://gitlab.com/prace-ri/hpc-kernels/sparse_linear_algebra)
 * [Spectral methods](https://gitlab.com/prace-ri/hpc-kernels/spectral_methods)
 * [Structured grids](https://gitlab.com/prace-ri/hpc-kernels/structured_grids)
 * [Unstructured grids](https://gitlab.com/prace-ri/hpc-kernels/unstructured_grids)

## Training material

### Parallel programming

 * [MPI, a short introduction](https://gitlab.com/prace-ri/training-material/parallel-programming/MPI)
 * [MPI, a complete MPI-3.1/4.0/... course with exercises and examples in C, Fortran and Python ...](https://www.hlrs.de/training/par-prog-ws/MPI-course-material)
 * [OpenMP](https://gitlab.com/prace-ri/training-material/parallel-programming/OpenMP)

### GPU Programming

 * [CUDA](https://gitlab.com/prace-ri/training-material/GPU-programming/CUDA)

### PGAS Languages

 * [Chapel](https://gitlab.com/prace-ri/training-material/PGAS-programming/chapel)
 * [GASPI](https://gitlab.com/prace-ri/training-material/PGAS-programming/gaspi)

### Best practices guides
 * [Matrix-vector multiplication Example (from serial to parallel)](https://gitlab.com/prace-ri/training-material/Matrix_vector_multiplication_From_Serial_to_Parallel)
